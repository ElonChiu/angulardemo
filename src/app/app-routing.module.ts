import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { ConfirmCheckComponent } from './confirm-check/confirm-check.component';
import { OtpComponent } from './otp/otp.component';
import { OrderCheckComponent } from './order-check/order-check.component';
import { ErrorComponent } from './error/error.component';

const routes: Routes = [
  { path: 'auth/:id', component: AuthComponent },
  { path: 'confirmCheck', component: ConfirmCheckComponent },
  { path: 'orderCheck', component: OrderCheckComponent },
  { path: 'verificationOTP', component: OtpComponent },
  { path: 'error', component: ErrorComponent },

  { path: '', redirectTo: 'error', pathMatch: 'full' },
  { path: '**', redirectTo: 'error' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
