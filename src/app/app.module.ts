import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { ConfirmCheckComponent } from './confirm-check/confirm-check.component';
import { OtpComponent } from './otp/otp.component';
import { OrderCheckComponent } from './order-check/order-check.component';
import { ErrorComponent } from './error/error.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { CustInfoComponent } from './cust-info/cust-info.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    ConfirmCheckComponent,
    OtpComponent,
    OrderCheckComponent,
    ErrorComponent,
    HeaderComponent,
    CustInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
