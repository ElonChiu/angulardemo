import { SaveCustomerService } from '../save-customer.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  custId: string;
  data: any;
  urlPath: string;
  authForm: FormGroup;

  constructor(private route: ActivatedRoute, private formBuild: FormBuilder,
    private router: Router, public dataSvc: DataService, public dataCustomer: SaveCustomerService, private http: HttpClient) {
    this.authForm = this.formBuild.group({
      mima: '',
    });
  }

  ngOnInit() {
    this.urlPath = this.route.snapshot.paramMap.get('id');
    this.dataSvc.getWeb0000(this.urlPath).subscribe(result => {
      const response: HttpResponse<any> = result;
      this.data = result.body;
      if (response.status !== 200) {
        console.log('...');
      }
      this.custId = this.data[0].custId;
    });
  }

  onSubmit() {
    console.log('good job', this.authForm.value);
    this.getApi0001(this.authForm.value.mima);
  }

  getApi0001(mima: string) {
    this.dataSvc.getWeb0001(this.urlPath, mima).subscribe(result => {
      result.body[0].url = this.urlPath;
      result.body[0].custId = this.custId;
      this.dataCustomer.changeMessage(result.body);
      this.checkMima(result.status);
    }, error => {
      console.log(error);
    }, () => {
      console.log('complete!');
    }
    );
  }

  checkMima(status: number) {
    if (status === 200) {
      this.router.navigate(['confirmCheck']);
    } else {
      console.log('...');
    }
    this.authForm.reset();
  }

  postApi0001(mima: any) {
    this.dataSvc.postWeb0001(this.urlPath, mima).subscribe(result => {
      result.body[0].url = this.urlPath;
      result.body[0].custId = this.custId;
      this.dataCustomer.changeMessage(result.body);
    });
  }
}
