import { SaveCustomerService } from '../save-customer.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cust-info',
  templateUrl: './cust-info.component.html',
  styleUrls: ['./cust-info.component.css']
})
export class CustInfoComponent implements OnInit {

  data: any;

  constructor(private route: ActivatedRoute, public dataCustomer: SaveCustomerService) {}

  ngOnInit() {
    this.dataCustomer.messageSource.subscribe(web0001data => {
      this.data = web0001data;
    });
  }

}
