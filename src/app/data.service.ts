import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  address: string;

  headers = new HttpHeaders({
    'Content-Type': 'text/json'
  });
  options = {
    headers: this.headers
  };

  constructor(private http: HttpClient) { }

  getWeb0000(urlSeq: string) {
    return this.http.get<any>('http://localhost:3000/web0000?urlSeq=' + urlSeq, { observe: 'response', responseType: 'json' });
  }

  getWeb0001(urlSeq: string, mima: string): Observable<any> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<any>('http://localhost:3000/web0001?urlSeq=' + urlSeq + '&mima=' + mima, { observe: 'response', responseType: 'json' });
  }

  postWeb0001(urlSeq: string, body: any) {
    const bodys = JSON.stringify(body);
    return this.http.post<any>('http://localhost:3000/web0001?urlSeq=' + urlSeq, bodys, this.options);
  }
}
