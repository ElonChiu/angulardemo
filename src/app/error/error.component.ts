import { Component, OnInit } from '@angular/core';
import { SaveCustomerService } from '../save-customer.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  data: any;

  constructor(public dataCustomer: SaveCustomerService) { }

  ngOnInit() {
    this.dataCustomer.messageSource.subscribe(web0001data => {
      this.data = web0001data;
    });

  }
}
