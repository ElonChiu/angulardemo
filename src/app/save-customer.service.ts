import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SaveCustomerService {

  constructor() { }

  public messageSource = new BehaviorSubject<any>('');


  changeMessage(getWeb0001): void {
    this.messageSource.next(getWeb0001);
  }
}
